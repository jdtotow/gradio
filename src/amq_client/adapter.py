import logging, time, os 
from threading import Thread
from amq_client.MorphemicConnection import Connection

activemq_port = int(os.environ.get("ACTIVEMQ_PORT", "61613"))
activemq_hostname = os.environ.get("ACTIVEMQ_HOST", "localhost")
activemq_topic = os.environ.get("ACTIVEMQ_TOPIC", "static-topic-1")
activemq_subs_key = os.environ.get("ACTIVEMQ_SUBS_KEY", "subs-1")
activemq_username = os.environ.get("ACTIVEMQ_USERNAME", "aaa")
activemq_password = os.environ.get("ACTIVEMQ_PASSWORD", "111")

#logname = "./log/ps.log"
#logging.basicConfig(filename=logname,filemode='a',format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',datefmt='%H:%M:%S',level=logging.DEBUG)

class Listener(object):
    def __init__(self, conn, handler, metadata):
        self.conn = conn
        self.count = 0
        self.metadata = metadata 
        self.handler = handler
        self.start = time.time()

    def on_error(self, frame):
        print("received an error %s" % frame.body)

    def on_message(self, frame):
        self.handler(frame.body, self.metadata)

class Consumer(Thread):
    def __init__(self, handler, queue, consumer_id = int(time.time()), metadata = {}):
        self.handler = handler 
        self.queue = queue 
        self.consumer_id = consumer_id
        self.metadata = metadata
        self.conn = None 
        self.stop_consumer = False
        super(Consumer,self).__init__()

    def stop(self):
        self.stop_consumer = True
        if self.conn: 
            self.conn.disconnect()

    def run(self):
        connected = False 
        while not connected:
            if self.stop_consumer:
                break 
            try:
                print('Subscribe to the topic {0}'.format(self.queue))
                #logging.info('Subscribe to the topic {0}'.format(self.queue))
                self.conn = Connection(username=activemq_username, password=activemq_password, host=activemq_hostname,port=activemq_port, debug=False)
                self.conn.connect()
                self.conn.set_listener('', Listener(self.conn, self.handler, self.metadata))
                self.conn.subscribe(destination=self.queue, id=self.consumer_id, ack='auto')
                connected = True 
            except Exception as e:
                print("Could not subscribe")
                #logging.error("Could not subscribe to the topic {0}".format(self.queue))
                print(e)
                connected = False

class Publisher(Thread):
    def __init__(self):
        self.message = None 
        self.destination = None 
        self.client = None 
        super(Publisher, self).__init__()

    def setParameters(self, message, queue):
        self.message = message
        self.queue = queue 

    def run(self):
        self.connect()
        while True:
            time.sleep(2)

    def connect(self):
        while True:
            try:
                print('The publisher tries to connect to ActiveMQ broker')
                #logging.info('The publisher tries to connect to ActiveMQ broker')
                self.client = Connection(username=activemq_username, password=activemq_password, host=activemq_hostname,port=activemq_port, debug=False)
                self.client.connect()
                print("connection established")
                #logging.info("connection established !!!")
                return True 
            except:
                print("Could not connect the publisher")
                #logging.error("Could not connect the publisher")

    def send(self):
        if self.message == None or self.queue == None:
            print("Message or queue is None")
            return False 
        if not self.client:
            time.sleep(5)
            self.send()
        try:
            #self.client.send(body=json.dumps(self.message), destination=self.queue, persistent='false', auto_content_length=False, content_type="application/json")
            self.client.send_to_topic(self.queue, self.message)
            return True 
        except Exception as e:
            print(e)
            self.client.disconnect()
            print("Reconnection in 10s ...")
            #logging.info("Reconnection in 10s ...")
            time.sleep(10)
            self.connect()
            self.send()