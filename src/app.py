import fastapi, gradio, os, timeit, xgboost, time, psutil
import sklearn.datasets
import sklearn.metrics
import sklearn.model_selection
import sklearn.preprocessing
from threading import Thread
from pathlib import Path 
from dotenv import load_dotenv
from amq_client.adapter import Publisher

#activemq_port = int(os.environ.get("ACTIVEMQ_PORT", "61613"))
#activemq_hostname = os.environ.get("ACTIVEMQ_HOST", "localhost")
environment_parameters_file = os.environ.get("ENV_FILE_PATH","/tmp/.env")
dotenv_path = Path(environment_parameters_file)
load_dotenv(dotenv_path=dotenv_path)

metric_export = os.environ.get("METRIC_EXPORT","disable")
execution_hardware = os.environ.get("MORPHEMIC_HARDWARE","CPU")


api = fastapi.FastAPI()
metrics = {}

def publish():
    global metrics
    publisher = Publisher()
    publisher.start()
    keys = [ "alpha", "eta", "max_depth", "response_time"]
    while True:
        process = psutil.Process(os.getpid())
        memory_usage = int(process.memory_info().rss / 1000000)
        processor_usage = psutil.cpu_percent()
        metrics["memory"] = memory_usage
        metrics["processor_usage"] = processor_usage
        for key in keys:
            if not key in metrics:
                metrics[key] = 0
        for metric_name, metric_value in metrics.items():
            data_to_send = {"metricValue": metric_value, "level": 0, "timestamp": int(time.time() * 1000),"component": "gradio"}
            publisher.setParameters(data_to_send, metric_name)
            publisher.send()
            print("Metrics published -> ", metrics)
        time.sleep(10)

if metric_export == "enable":
    t = Thread(target= publish, args=())
    t.start()
        
def fn(alpha, eta, max_depth, name, num_boost_round, subsample, test_size,tree_method):
    global metrics
    if tree_method == 'auto':
        if execution_hardware == 'CPU':
            tree_method = 'exact'
        if execution_hardware == 'FPGA':
            tree_method = 'fpga_exact'

    X, y = sklearn.datasets.fetch_openml(name, return_X_y=True)

    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(
        X, y, test_size=test_size)

    normalizer = sklearn.preprocessing.Normalizer()
    X_train = normalizer.fit_transform(X_train)
    X_test = normalizer.transform(X_test)

    label_encoder = sklearn.preprocessing.LabelEncoder()
    y_train = label_encoder.fit_transform(y_train)
    y_test = label_encoder.transform(y_test)

    xgb = xgboost.XGBClassifier(n_estimators=num_boost_round,
                                max_depth=max_depth,
                                learning_rate=eta,
                                objective='multi:softmax',
                                tree_method=tree_method,
                                subsample=subsample,
                                reg_alpha=alpha)

    start = timeit.default_timer()
    model = xgb.fit(X_train, y_train)
    stop = timeit.default_timer()
    duration = stop - start
    print('time=%.3f' % duration)

    if metric_export == "enable":
        process = psutil.Process(os.getpid())
        memory_usage = int(process.memory_info().rss / 1000000)
        processor_usage = psutil.cpu_percent()

        metrics = {
            "alpha": alpha,
            "eta": eta, 
            "memory": memory_usage,
            "processor_usage": processor_usage,
            "max_depth": max_depth,
            "response_time": duration
        }
        

    predictions = model.predict(X_test)
    return {'accuracy': sklearn.metrics.accuracy_score(y_test, predictions)}


with gradio.Blocks(analytics_enabled=False,
                   title='XGBoost Classifier') as blocks:
    gradio.Markdown(
        value='<h1 style="text-align: center; margin-bottom: 1rem">' +
        blocks.title + '</h1>')
    with gradio.Row():
        with gradio.Column():
            alpha = gradio.Number(0.0,
                                  label='L1 regularization term on weights')
            eta = gradio.Slider(
                0.0,
                1.0,
                0.3,
                label=
                'Step size shrinkage used in update to prevent overfitting')
            max_depth = gradio.Number(6,
                                      label='Maximum depth of a tree',
                                      precision=0)
            name = gradio.Textbox(
                max_lines=1,
                label='String identifier of the dataset (https://openml.org)',
                info='e.g. CIFAR_10, Fashion-MNIST, mnist_784, SVHN, etc.')
            num_boost_round = gradio.Number(
                10, label='Number of boosting iterations', precision=0)
            subsample = gradio.Slider(
                0.01,
                1.0,
                1.0,
                label='Subsample ratio of the training instances')
            test_size = gradio.Slider(
                0.1,
                0.9,
                0.25,
                step=0.05,
                label=
                'The proportion of the dataset to include in the test split')
            tree_method = gradio.Dropdown(
                ['auto', 'exact', 'approx', 'hist', 'gpu_hist', 'fpga_exact'],
                value='auto',
                label='The tree construction algorithm used in XGBoost')
        with gradio.Column():
            classification_report = gradio.Label(label='Classification Report')
            gradio.Button(value='Train').click(fn,
                                               inputs=[
                                                   alpha, eta, max_depth, name,
                                                   num_boost_round, subsample,
                                                   test_size, tree_method
                                               ],
                                               outputs=[classification_report],
                                               api_name='api')
    gradio.Examples(examples='examples',
                    inputs=[
                        alpha, eta, max_depth, name, num_boost_round,
                        subsample, test_size, tree_method
                    ])

api = gradio.mount_gradio_app(api, blocks, path='/gradio')
