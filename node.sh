#!/bin/sh
curl -sS https://get.docker.com | sh
sudo usermod -aG docker ${USER}
curl -sS https://setup.inaccel.com/repository | sh -s install
sudo systemctl enable --now inaccel
sudo docker inaccel run driver
sudo docker inaccel -e license=${LICENSE} up