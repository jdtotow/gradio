FROM python:3.9

COPY ./requirements.txt ./requirements.txt 
RUN pip install --upgrade pip 
RUN pip install -r ./requirements.txt 

RUN curl -sS https://setup.inaccel.com/repository | sh \
 && apt install --yes coral-api \
 && rm --force --recursive /var/lib/apt/lists/*

RUN git clone --branch coral --depth 1 --recursive https://github.com/inaccel/xgboost.git \
 && make -C xgboost patch \
 && make -C xgboost/xgboost all \
 && cd xgboost/xgboost/python-package \
 && python setup.py install \
 && cd ../../.. \
 && rm -rf xgboost
 
ADD ./src /app/src  
WORKDIR /app/src 
EXPOSE 8080
#COPY examples examples
ENTRYPOINT ["uvicorn", "--host=0.0.0.0", "--port=8080", "app:api"]
